#ifndef CONFIG_H
#define CONFIG_H

#include <string>


class Config
{
    public:
        Config();
        virtual ~Config();

    protected:

    private:
        std::string path = "~/.config/accounts_transfer";
        std::string file = "settings.conf";
};

#endif // CONFIG_H
