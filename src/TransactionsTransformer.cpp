#include "TransactionsTransformer.h"
#include "Ledger.h"
#include <vector>
#include <iostream>
#include <regex>
#include <stdexcept>
#include <fstream>

/***************************************************************************************************
 * Public Methods
 **************************************************************************************************/
void TransactionsTransformer::process(std::vector<BankStatement::transactionEntry> sourceTxs)
{
    buildDictionary();

    for (BankStatement::transactionEntry txSource: sourceTxs) {
        addToLedger(txSource);
    }
}

std::string TransactionsTransformer::output()
{
    return ledger.output();
}

/***************************************************************************************************
 * Private Methods
 **************************************************************************************************/
void TransactionsTransformer::buildDictionary()
{
    std::ifstream ifLedgerTranslationsFile;
    std::string   line;
    
    ifLedgerTranslationsFile.open("ledger-dictionary.txt", std::ios_base::in);

    while(std::getline(ifLedgerTranslationsFile,line)) {
        // Ignore comments and empty lines
        if (line == "" || line[0] == '#') {
            continue;
        }
        
        const char delim = ';';
        
        std::vector<std::string> items = split(line, delim);
        
        if (items.size() != 3) {
            throw std::invalid_argument("Cannot process line from input file '" + line + "'");
        }
        
        txDictEntry entry;
        
        entry.sourceRegex = std::regex(items[0]);
        entry.category    = items[1];
        entry.title       = items[2];
        
        transactionCategories.push_back(entry);
    }
}

std::vector<std::string> TransactionsTransformer::split(const std::string& s, char delimiter)
{
   std::vector<std::string> tokens;
   
   std::string        token;
   std::istringstream tokenStream(s);
   
   while (std::getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   
   return tokens;
}

void TransactionsTransformer::addToLedger(BankStatement::transactionEntry sourceTx)
{
    Ledger::transaction tx;
    
    txDictEntry entry   = getPaymentToCategory(sourceTx);
    
    tx.amount           = sourceTx.amount;
    tx.date             = dmyToYmd(sourceTx.date);
    tx.isConfirmed      = true;
    tx.description      = entry.title == "" ? sourceTx.description : replaceTitlePlaceholder(entry.title, sourceTx);
    tx.payFromCat       = (sourceTx.accountNumber.substr(10, 1) == "2") ? "assets:current" : "assets:isa";
    tx.payToCat         = entry.category;
    
    if (tx.payToCat == "DUPLICATE" || tx.payToCat == "VOID")
        return;
    
    ledger.addTransaction(tx);
}

std::string TransactionsTransformer::dmyToYmd(const std::string &dmy) {
    return dmy.substr(6, 4) + "/" + dmy.substr(3, 2) + "/" + dmy.substr(0, 2);
}

std::string TransactionsTransformer::replaceTitlePlaceholder(const std::string &title, const BankStatement::transactionEntry &sourceTx)
{
    std::string strRepl           = "%s";
    std::string cstrTitle         = title;

    long unsigned int pos;

    while ((pos = cstrTitle.find(strRepl)) != std::string::npos)
        cstrTitle.replace(pos, strRepl.length(), sourceTx.description + " (" + sourceTx.method + ")");
    
    return cstrTitle;
}

TransactionsTransformer::txDictEntry TransactionsTransformer::getPaymentToCategory(BankStatement::transactionEntry tx)
{
    std::smatch matches;
    
    for ( txDictEntry entry : transactionCategories ) {
        if (std::regex_match(tx.description, matches, entry.sourceRegex)) {
            return entry;
        }
        
        // Also check methods (POS, C/L, etc)
        if (std::regex_match(tx.method, matches, entry.sourceRegex)) {
            return entry;
        }
    }

    txDictEntry fallBack;
    
    fallBack.title    = "";
    fallBack.category = "payments:unknown";
    
    return fallBack;
}
