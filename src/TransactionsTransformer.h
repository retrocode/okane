#ifndef TRANSACTIONSTRANSFORMER_H
#define TRANSACTIONSTRANSFORMER_H

#include <string>
#include <vector>
#include <map>
#include <regex>
#include "BankStatement.h"
#include "Ledger.h"

class TransactionsTransformer {
public:
    void process(std::vector<BankStatement::transactionEntry> sourceTxs);
    std::string output();

protected:

private:
    Ledger ledger;
    struct txDictEntry {
        std::regex sourceRegex;
        std::string category;
        std::string title;
    };

    void addToLedger(BankStatement::transactionEntry sourceTx);
    void buildDictionary();
    std::vector<std::string> split(const std::string&, char);
    TransactionsTransformer::txDictEntry  getPaymentToCategory(BankStatement::transactionEntry tx);
    std::vector<txDictEntry> transactionCategories;
    std::string replaceTitlePlaceholder(const std::string& title, const BankStatement::transactionEntry& sourceTx);
    std::string dmyToYmd(const std::string &dmy);
};

#endif // TRANSACTIONSTRANSFORMER_H
