#ifndef LEDGER_H
#define LEDGER_H

#include <string>
#include <vector>

class Ledger {
public:
    struct transaction {
        std::string date;
        bool isConfirmed;
        std::string description;
        std::string payToCat;
        float amount;
        std::string payFromCat;
    };
    void addTransaction(transaction);
    std::string output();
private:
    std::vector<transaction> transactions;
    static std::string formatTx(transaction);

};

#endif // LEDGER_H
