#include <iostream>
#include <vector>

#include "AccountsParserConfig.h"
#include "BankStatement.h"
#include "TransactionsTransformer.h"

int main(int argc,  char *argv[])
{
    if (argc < 2) {
        fprintf(stdout,"%s Version %d.%d\n",
            argv[0],
            Tutorial_VERSION_MAJOR,
            Tutorial_VERSION_MINOR);

        std::cout << "Usage: %s path/to/bank-statements.csv\n" << argv[0];
        return 1;
    };

    std::string filename = argv[1];

    // Process source file and extract transactions
    BankStatement sourceBankStatements(filename);
    std::vector<BankStatement::transactionEntry> txs = sourceBankStatements.extractTransactions();
    std::cout << "# Total transactions: " << txs.size() << "\n";

    // Perform conversion on imported data
    TransactionsTransformer transformer;
    transformer.process(txs);
    // TODO: Output the initial balance for accounts, optionally...
    std::cout << transformer.output();

    return 0;
}
