#ifndef ACCOUNTSFILE_H
#define ACCOUNTSFILE_H

#include <string>
#include <vector>

class BankStatement
{

public:
    explicit BankStatement ( const std::string &filename );
    virtual ~BankStatement();
    struct transactionEntry {
        std::string date;
        std::string method; // POS, D/D, S/O
        std::string description;
        float amount;
        float balance;
        std::string accountUser;
        std::string accountNumber;
    };
    std::vector<BankStatement::transactionEntry> extractTransactions();


protected:

private:
    std::string strFilename;
};

#endif // ACCOUNTSFILE_H
