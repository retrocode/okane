#include "Ledger.h"

#include <iostream>
#include <algorithm>
#include <numeric>
#include <sstream>

void Ledger::addTransaction(Ledger::transaction tx)
{
    Ledger::transactions.push_back(tx);
}

std::string Ledger::output()
{
    std::vector<std::string> result;

    transform(transactions.begin(), transactions.end(), std::back_inserter(result), Ledger::formatTx);

    return std::accumulate(result.begin(), result.end(), std::string(""));
}

std::string Ledger::formatTx(Ledger::transaction tx)
{
    std::ostringstream formattedAmount;
    formattedAmount << tx.amount;

    std::string formatted = tx.date + (tx.isConfirmed ? " ! " : " * ") + tx.description + "\n"
                            + "    " + tx.payFromCat + "  \t" + formattedAmount.str() + "\n"
                            + "    " + tx.payToCat + "\n\n";


    return formatted;
}
