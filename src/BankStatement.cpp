#include "BankStatement.h"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

BankStatement::BankStatement(const std::string &filename): strFilename(filename)
{}

BankStatement::~BankStatement()
{
    //dtor
}


std::vector<BankStatement::transactionEntry> BankStatement::extractTransactions()
{
    std::vector<BankStatement::transactionEntry> txList;
    std::ifstream  data(strFilename);
    std::string line;
    std::string cellContent;
    bool isQuoted = false;

    while(std::getline(data,line)) {
        std::stringstream  lineStream(line);
        std::string        cell;
        std::vector<std::string> elements;

        // Extract elements in a line (comma separated)
        while(std::getline(lineStream,cell,',')) {
            if (cell[0] == '"') {
                isQuoted = true;
            }

            cellContent = isQuoted ? cellContent + cell : cell;

            if (cell.back() == '"') {
                isQuoted = false;
            }

            if (isQuoted == false) {
                if (cellContent.front() == '"')
                    cellContent.erase(0, 1);
                if (cellContent.front() == '\'')
                    cellContent.erase(0, 1);
                if (cellContent.back() == '"')
                    cellContent.erase(cellContent.size() - 1);

                elements.push_back(cellContent);
                cellContent = "";
            }

        }

        if (elements.size() >= 7) {
            transactionEntry tx;
            tx.date = elements[0];

            tx.method = elements[1];
            tx.description = elements[2];

            try {
                tx.amount = stod(elements[3]);
                tx.balance = stod(elements[4]);
            }
            catch (std::exception& e) {
                // Could not convert string to float - probably this is a header row
                continue;
            }
            tx.accountUser = elements[5];
            tx.accountNumber = elements[6];
            txList.push_back(tx);
        }
    }

    return txList;
}
