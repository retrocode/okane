# Okane

This is a simple script to convert bank format CSV export files in to a format that's compatible with [Ledger](https://www.ledger-cli.org/)

## Downloading Transactions

* Login
* Go to `Statements`
* Scroll down to `Download or export transactions` and click link
* Select a time period and
* Download/export type `Excel, Lotus 123, Text (CSV)`


## Preparation:

Build the `accounts-parser` with `make`, and copy the `ledger-dictionary.txt.sample` to `ledger-dictionary.txt`.  This file is expected to exist in the current folder (usually the same folder as the executable).

As it's not a script that's expected to be used frequently, there isn't an installer.  This may be refined at a later date.

The format of the source CSV file is based simply on the output from my bank, and is a Sage compatible format.  There is currently no checking on which columns are present or matching specific columns - it purely assumes that they are in a set order.

Usage: `accounts-parser path/to/bank-csv-export.csv`

## Sample output:

```
2019/03/07 ! Amazon Book
    assets:current  	-7.64
    expenses:luxuries:books

2019/03/07 ! Transfer
    assets:current  	-70
    assets:isa

2019/03/08 ! Utilities
    assets:current  	-101
    expenses:bills:utilities

2019/03/08 ! Costa Coffee
    assets:current  	-4.5
    expenses:misc:dining
```

## Alternatives

There is also a perfectly capable alternative to this tool at: https://hledger.org/csv.html which is much more flexible and powerful, although from my point of view is more complex and harder to set-up.  However, it may be more likely to suit your needs.

